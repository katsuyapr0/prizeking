//
//  CustomButton.m
//  PrizeKing
//
//  Created by Andrés Abril on 22/07/12.
//
//

#import "CustomButton.h"

@implementation CustomButton
@synthesize actionID;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
