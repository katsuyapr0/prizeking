//
//  NavAnimations.h
//  PrizeKing
//
//  Created by Andres Abril on 22/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@interface NavAnimations : NSObject

+(CATransition*)navAlphaAnimation;
@end
