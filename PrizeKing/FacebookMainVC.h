//
//  FacebookMainVC.h
//  PrizeKing
//
//  Created by Andrés Abril on 11/08/12.
//
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "Friend.h"
#import "NavAnimations.h"
#import "MyTeamVC.h"
#import "FacebookMainVC.h"
#import "MyInvitations.h"
#import "FriendsTeamVC.h"

@interface FacebookMainVC : UIViewController{

}
@property(nonatomic,retain)User *user;

@end
