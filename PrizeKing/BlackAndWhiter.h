//
//  BlackAndWhiter.h
//  PrizeKing
//
//  Created by Andrés Abril on 27/08/12.
//
//

#import <Foundation/Foundation.h>

@interface BlackAndWhiter : NSObject

+(UIImage*)turnImageToBlackAndWhite:(UIImage*)image;

@end
