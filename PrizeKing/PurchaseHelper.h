//
//  PurchaseHelper.h
//  PrizeKing
//
//  Created by Andrés Abril on 17/07/12.
//
//

#import <Foundation/Foundation.h>
#import "PurchaseManager.h"

@interface PurchaseHelper : PurchaseManager {
    
}

+ (PurchaseManager *) sharedHelper;

@end

