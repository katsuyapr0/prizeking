//
//  Friend.h
//  PrizeKing
//
//  Created by Andres Abril on 28/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Friend : NSObject
@property(nonatomic,retain)NSString *name;
@property(nonatomic,retain)NSString *ID;
@property(nonatomic,retain)NSString *installed;

@end
